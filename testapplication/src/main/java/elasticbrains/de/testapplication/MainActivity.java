package elasticbrains.de.testapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ase52.com.test_library.TestClass;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
